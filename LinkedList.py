
class Item:

    def __init__(self, value):
        self.value = value;
        self.next = None;
        self.previous = None;

    def setNext(self, item):
        self.next = item;

    def setPrevious(self, item):
        self.previous = item;

    def setValue(self, value):
        self.value = value;

class LinkedList:

    def __init__(self):
        self.length = 0;
        self.head = None;
        self.tail = None;
        self.current = None;

    def addAfterTail(self, value):
        item = Item(value);

        if (self.tail != None):
            self.tail.setNext(item);
            item.setPrevious(self.tail);

        self.length += 1;
        self.tail = item;

        if (self.length == 1):
            self.head = item;

        return item;

    def addBeforeHead(self, value):
        item = Item(value);

        if (self.head != None):
            self.head.setPrevious(item);
            item.setNext(self.head);

        self.length += 1;
        self.head = item;

        if (self.length == 1):
            self.tail = item;

        return item;

    def addAfterIndex(self, index, value):
        itemOnIndex = self.get(index);

        if (itemOnIndex == None):
            raise Exception('Index not found in list');
        
        return self.addAfter(itemOnIndex, value);

    def addBeforeIndex(self, index, value):
        itemOnIndex = self.get(index);

        if (itemOnIndex == None):
            raise Exception('Index not found in list');
        
        return self.addBefore(itemOnIndex, value);

    def addAfter(self, item, value):
        valueItem = Item(value);

        valueItem.setPrevious(item);
        valueItem.setNext(item.next);

        if (item.next != None):
            item.next.setPrevious(valueItem);

        item.setNext(valueItem);

        if (item == self.tail):
            self.tail = valueItem;

        self.length += 1;
        
        return valueItem;

    def addBefore(self, item, value):
        valueItem = Item(value);

        valueItem.setNext(item);
        valueItem.setPrevious(item.previous);

        if (item.previous != None):
            item.previous.setNext(valueItem);

        item.setPrevious(valueItem);

        if (item == self.head):
            self.head = valueItem;

        self.length += 1;
        
        return valueItem;


    def get(self, index):
        if (self.length == 0):
            return None;
        
        currentItem = self.head;
        count = 0;

        while (count < index):
            if (currentItem.next == None):
                return None;

            currentItem = currentItem.next;
            count += 1;
        
        return currentItem;

    def removeItem(self, item):

        if (item.next != None):
            item.next.setPrevious(item.previous);
        else:
            self.tail = item.previous;
        
        if (item.previous != None):
            item.previous.setNext(item.next);
        else:
            self.head = item.next;

        item.setNext(None);
        item.setPrevious(None);

        self.length -= 1;

    def setCurrent(self, item):
        self.current = item;

    def addAfterCurrent(self, value):
        if (self.current == None):
            raise Exception("Current pointer has not been set");
        
        return self.addAfter(self.current, value);

    def addBeforeCurrent(self, value):
        if (self.current == None):
            raise Exception("Current pointer has not been set");
        
        return self.addBefore(self.current, value);