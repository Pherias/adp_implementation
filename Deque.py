from LinkedList import LinkedList;

class Deque:

    def __init__(self):
        self.list = LinkedList();
        self.length = 0;
    
    def insertRight(self, value):
        self.list.addAfterTail(value);
        self.length += 1;

    def insertLeft(self, value):
        self.list.addBeforeHead(value);
        self.length += 1;

    def removeLeft(self):
        self.list.removeItem(self.list.head);
        self.length -= 1;

    def removeRight(self):
        self.list.removeItem(self.list.tail);
        self.length -= 1;
    
    def peakRight(self):
        if (self.length == 0):
            raise Exception("Deque is empty");

        return self.list.tail.value;

    def peakLeft(self):
        if (self.length == 0):
            raise Exception("Deque is empty");

        return self.list.head.value;



    