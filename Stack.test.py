import unittest

from Stack import Stack;

class TestStack(unittest.TestCase):
    
    def test_addingItems(self):
        stack = Stack();

        stack.add('test1');
        stack.add('test2');
        stack.add('test3');

        self.assertEqual(stack.peak(), 'test3');
        self.assertEqual(stack.length, 3);

    def test_poppingItems(self):
        stack = Stack();

        stack.add('test1');
        stack.add('test2');
        stack.add('test3');

        firstPoppedItem = stack.pop();
        secondPoppedItem = stack.pop();

        self.assertEqual(stack.length, 1);
        self.assertEqual(stack.peak(), 'test1');
        self.assertEqual(firstPoppedItem, 'test3');
        self.assertEqual(secondPoppedItem, 'test2');

    def test_isEmpty(self):
        stack = Stack();

        stack.add('test1');
        stack.add('test2');
        stack.add('test3');

        stack.pop();
        stack.pop();
        stack.pop();

        self.assertEqual(stack.isEmpty(), True);

if __name__ == '__main__':
    unittest.main()