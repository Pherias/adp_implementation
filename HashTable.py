class HashTable:

    def __init__(self):
        self.items = [None] * 10;

    def set(self, key, value):
        keyIndex = self.getHash(key);

        while len(self.items) < keyIndex:
            self.increaseItemsLength();

        if (self.items[keyIndex] == None):
            self.items[keyIndex] = [Item(key, value)];
        else:
            hasKey = False;
            for i in range(0, len(self.items[keyIndex])):
                if self.items[keyIndex][i].key == key:
                    self.items[keyIndex][i].value = value;
                    hasKey = True;
            
            if hasKey == False:
                self.items[keyIndex].append(Item(key, value));

    def get(self, key):
        keyIndex = self.getHash(key);

        if len(self.items) < keyIndex:
            raise Exception("Key does not exist");

        if self.items[keyIndex] == None:
            raise Exception("Key does not exist");

        for i in range(0, len(self.items[keyIndex])):
            if self.items[keyIndex][i].key == key:
                return self.items[keyIndex][i].value;
        
        raise Exception("Key does not exist");

    def delete(self, key):
        keyIndex = self.getHash(key);

        if len(self.items) < keyIndex:
            raise Exception("Key does not exist");

        if self.items[keyIndex] == None:
            raise Exception("Key does not exist");

        hasKey = False;
        for i in range(0, len(self.items[keyIndex])):
            if self.items[keyIndex][i].key == key:
                self.items[keyIndex][i:1] = []
                hasKey = True;
                break;
        
        if hasKey == False:
            raise Exception("Key does not exist");

    # Get a hashed value from a string to serve as an index in the array.
    def getHash(self, key):
        hash = 0
        for letter in key: 
            hash = (hash + ord(letter) * 23) % len(self.items)
        return hash

    def increaseItemsLength(self):
        newItems = [None] * (len(self.items) * 2);

        index = 0;

        for item in self.items:
            newItems[index] = item;
            index += 1;


class Item:
    def __init__(self, key, value):
        self.key = key;
        self.value = value;