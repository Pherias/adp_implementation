from logging import exception
import unittest

from LinkedList import LinkedList;

class TestLinkedList(unittest.TestCase):
    
    def test_addingOneItemToTail(self):
        list = LinkedList();

        addedItem = list.addAfterTail('test1');

        self.assertEqual(addedItem.next, None);
        self.assertEqual(addedItem.previous, None);
        self.assertEqual(list.length, 1);
        self.assertEqual(list.head.value, 'test1');
        self.assertEqual(list.tail.value, 'test1');

    def test_addingMultipleItemsToTail(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next, secondItem);
        self.assertEqual(secondItem.next, thirdItem);
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous, firstItem);
        self.assertEqual(thirdItem.previous, secondItem);

        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(list.head.value, 'test1');

    def test_addingMultipleItemsBeforeHead(self):
        list = LinkedList();

        firstItem = list.addBeforeHead('test1');
        secondItem = list.addBeforeHead('test2');
        thirdItem = list.addBeforeHead('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(thirdItem.next, secondItem);
        self.assertEqual(secondItem.next, firstItem);
        self.assertEqual(firstItem.next, None)

        self.assertEqual(thirdItem.previous, None);
        self.assertEqual(secondItem.previous, thirdItem);
        self.assertEqual(firstItem.previous, secondItem);

        self.assertEqual(list.tail.value, 'test1');
        self.assertEqual(list.head.value, 'test3');

    def test_addingAfterSpecificIndex(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next, secondItem);
        self.assertEqual(secondItem.next, thirdItem);
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous, firstItem);
        self.assertEqual(thirdItem.previous, secondItem);

        addedItem = list.addAfterIndex(1, 'test4');

        self.assertEqual(list.length, 4);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test4');
        self.assertEqual(addedItem.next.value, 'test3');
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(addedItem.previous.value, 'test2');
        self.assertEqual(thirdItem.previous.value, 'test4');

        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(list.head.value, 'test1');

    def test_addingAfterSpecificIndexOnTail(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next, secondItem);
        self.assertEqual(secondItem.next, thirdItem);
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous, firstItem);
        self.assertEqual(thirdItem.previous, secondItem);

        addedItem = list.addAfterIndex(2, 'test4');

        self.assertEqual(list.length, 4);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test3');
        self.assertEqual(thirdItem.next.value, 'test4');
        self.assertEqual(addedItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(thirdItem.previous.value, 'test2');
        self.assertEqual(addedItem.previous.value, 'test3');

        self.assertEqual(list.tail.value, 'test4');
        self.assertEqual(list.head.value, 'test1');

    def test_addingAfterNonExistingIndex(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next, secondItem);
        self.assertEqual(secondItem.next, thirdItem);
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous, firstItem);
        self.assertEqual(thirdItem.previous, secondItem);

        self.assertRaises(Exception, list.addAfterIndex, 3, 'test4');

        fourthItem = list.addAfterTail('test4');
        fifthItem = list.addAfterIndex(3, 'test5')

        self.assertEqual(fifthItem.next, None)
        self.assertEqual(fifthItem.previous.value, 'test4')
        self.assertEqual(fourthItem.next.value, 'test5')
        self.assertEqual(fourthItem.previous.value, 'test3')
        self.assertEqual(thirdItem.next.value, 'test4')

    def test_addingBeforeSpecificIndex(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        addedItem = list.addBeforeIndex(1, 'test4');

        self.assertEqual(list.length, 4);
        self.assertEqual(firstItem.next.value, 'test4');
        self.assertEqual(addedItem.next.value, 'test2');

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(addedItem.previous.value, 'test1');
        self.assertEqual(secondItem.previous.value, 'test4');

        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(list.head.value, 'test1');

    def test_addingBeforeSpecificIndexOnHead(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        addedItem = list.addBeforeIndex(0, 'test4');

        self.assertEqual(list.length, 4);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test3');
        self.assertEqual(thirdItem.next, None);
        self.assertEqual(addedItem.next.value, 'test1');
        self.assertEqual(addedItem.previous, None);

        self.assertEqual(firstItem.previous.value, 'test4');
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(thirdItem.previous.value, 'test2');
        self.assertEqual(addedItem.previous, None);

        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(list.head.value, 'test4');

    def test_addingBeforeNonExistingIndex(self):
        list = LinkedList();

        list.addAfterTail('test1');
        list.addAfterTail('test2');
        list.addAfterTail('test3');

        self.assertRaises(Exception, list.addBeforeIndex, 3, 'test4');

    def test_addingAfterCurrentPointer(self):
        list = LinkedList();

        item1 = list.addAfterTail('test1');
        item2 = list.addAfterTail('test2');
        item3 = list.addAfterTail('test3');

        list.setCurrent(list.head.next);

        item4 = list.addAfterCurrent('test4');

        self.assertEqual(item1.next.value, 'test2');
        self.assertEqual(item2.next.value, 'test4');
        self.assertEqual(item4.next.value, 'test3');
        self.assertEqual(item3.next, None);

        self.assertEqual(item1.previous, None);
        self.assertEqual(item2.previous.value, 'test1');
        self.assertEqual(item4.previous.value, 'test2');
        self.assertEqual(item3.previous.value, 'test4');

    def test_addingAfterCurrentPointer(self):
        list = LinkedList();

        item1 = list.addAfterTail('test1');
        item2 = list.addAfterTail('test2');
        item3 = list.addAfterTail('test3');

        list.setCurrent(list.head.next);

        item4 = list.addBeforeCurrent('test4');

        self.assertEqual(item1.next.value, 'test4');
        self.assertEqual(item2.next.value, 'test3');
        self.assertEqual(item4.next.value, 'test2');
        self.assertEqual(item3.next, None);

        self.assertEqual(item1.previous, None);
        self.assertEqual(item2.previous.value, 'test4');
        self.assertEqual(item4.previous.value, 'test1');
        self.assertEqual(item3.previous.value, 'test2');

    def test_getIndexInList(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');
        fourthItem = list.addBeforeHead('test4');

        self.assertEqual(list.get(0), fourthItem);
        self.assertEqual(list.get(1), firstItem);
        self.assertEqual(list.get(2), secondItem);
        self.assertEqual(list.get(3), thirdItem);
    
    def test_nonExistingIndexGivesNone(self):
        list = LinkedList();

        list.addAfterTail('test1');
        list.addAfterTail('test2');
        list.addAfterTail('test3');
        
        self.assertEqual(list.get(3), None)

    def test_removeFirstItemFromList(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test3');
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(thirdItem.previous.value, 'test2');

        self.assertEqual(list.head, firstItem);
        self.assertEqual(list.tail, thirdItem);

        list.removeItem(firstItem);

        self.assertEqual(list.head.value, 'test2');
        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(secondItem.previous, None);
        
        self.assertEqual(firstItem.next, None)
        self.assertEqual(firstItem.previous, None)

        self.assertEqual(list.length, 2);

    def test_removeMiddleItemFromList(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test3');
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(thirdItem.previous.value, 'test2');

        self.assertEqual(list.head, firstItem);
        self.assertEqual(list.tail, thirdItem);

        list.removeItem(secondItem);

        self.assertEqual(list.head.value, 'test1');
        self.assertEqual(list.tail.value, 'test3');
        self.assertEqual(firstItem.next.value, 'test3');
        self.assertEqual(thirdItem.previous.value, 'test1');
        
        self.assertEqual(secondItem.next, None)
        self.assertEqual(secondItem.previous, None)

        self.assertEqual(list.length, 2);

    def test_removeLastItemFromList(self):
        list = LinkedList();

        firstItem = list.addAfterTail('test1');
        secondItem = list.addAfterTail('test2');
        thirdItem = list.addAfterTail('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(firstItem.next.value, 'test2');
        self.assertEqual(secondItem.next.value, 'test3');
        self.assertEqual(thirdItem.next, None);

        self.assertEqual(firstItem.previous, None);
        self.assertEqual(secondItem.previous.value, 'test1');
        self.assertEqual(thirdItem.previous.value, 'test2');

        self.assertEqual(list.head, firstItem);
        self.assertEqual(list.tail, thirdItem);

        list.removeItem(thirdItem);

        self.assertEqual(list.head.value, 'test1');
        self.assertEqual(list.tail.value, 'test2');
        self.assertEqual(secondItem.next, None);
        
        self.assertEqual(thirdItem.next, None)
        self.assertEqual(thirdItem.previous, None)

        self.assertEqual(list.length, 2);

    

if __name__ == '__main__':
    unittest.main()