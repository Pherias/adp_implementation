import unittest
import random

from List import List;

class TestList(unittest.TestCase):
    
    def test_binarySearchIntegers(self):
        list = List();

        list.add(5);
        list.add(1);
        list.add(7);
        list.add(4);
        list.add(2);
        list.add(16);
        list.add(15);
        list.add(12);
        list.add(10);

        def comparator(a, b):
            return a > b;

        list.insertionSort(comparator);

        self.assertEqual(list.binarySearch(3), False);
        self.assertEqual(list.binarySearch(12), True);
        self.assertEqual(list.binarySearch(15), True);
        self.assertEqual(list.binarySearch(17), False);

    def test_binarySearchIntegersLongSet(self):
        list = List();

        for i in range(1, 999):
            list.add(random.randrange(1, 100));

        list.add(112);
        list.add(142);

        def comparator(a, b):
            return a > b;

        list.insertionSort(comparator);

        self.assertEqual(list.binarySearch(142), True);
        self.assertEqual(list.binarySearch(112), True);
        self.assertEqual(list.binarySearch(101), False);


if __name__ == '__main__':
    unittest.main()