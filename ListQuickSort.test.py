import unittest

from List import List;

class TestList(unittest.TestCase):
    
    def test_quickSortIntegers(self):
        list = List();

        list.add(5);
        list.add(1);
        list.add(7);
        list.add(6);
        list.add(8);
        list.add(3);

        list.quickSort();

        self.assertEqual(list.get(0), 1);
        self.assertEqual(list.get(1), 3);
        self.assertEqual(list.get(2), 5);
        self.assertEqual(list.get(3), 6);
        self.assertEqual(list.get(4), 7);
        self.assertEqual(list.get(5), 8);

    def test_quickSortFloats(self):
        list = List();

        list.add(5.343);
        list.add(1.222);
        list.add(7.66423);
        list.add(6.3434);
        list.add(6.543);
        list.add(1.221);

        list.quickSort();

        self.assertEqual(list.get(0), 1.221);
        self.assertEqual(list.get(1), 1.222);
        self.assertEqual(list.get(2), 5.343);
        self.assertEqual(list.get(3), 6.3434);
        self.assertEqual(list.get(4), 6.543);
        self.assertEqual(list.get(5), 7.66423);

if __name__ == '__main__':
    unittest.main()