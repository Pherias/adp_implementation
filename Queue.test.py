import unittest

from Queue import Queue;

class TestQueue(unittest.TestCase):
    
    def test_enqueueingItems(self):
        queue = Queue();

        queue.enqueue('test1');
        queue.enqueue('test2');
        queue.enqueue('test3');

        self.assertEqual(queue.peak(), 'test1');
        self.assertEqual(queue.length, 3);

    def test_poppingItems(self):
        queue = Queue();

        queue.enqueue('test1');
        queue.enqueue('test2');
        queue.enqueue('test3');

        firstQueuedItem = queue.dequeue();
        secondQueuedItem = queue.dequeue();

        self.assertEqual(queue.length, 1);
        self.assertEqual(queue.peak(), 'test3');
        self.assertEqual(firstQueuedItem, 'test1');
        self.assertEqual(secondQueuedItem, 'test2');

    def test_isEmpty(self):
        queue = Queue();

        queue.enqueue('test1');
        queue.enqueue('test2');
        queue.enqueue('test3');

        queue.dequeue();
        queue.dequeue();
        queue.dequeue();

        self.assertEqual(queue.isEmpty(), True);

if __name__ == '__main__':
    unittest.main()