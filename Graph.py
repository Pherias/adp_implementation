class Graph():

	def __init__(self):
		self.dataIsSet = False;

	def setUsingAdjacencyMatrix(self, adjacencyMatrix):
		self.dataIsSet = True;
		
		self.nodesAmount = len(adjacencyMatrix);
		self.adjacencyMatrix = adjacencyMatrix;

	def setUsingLineList(self, lineList):
		self.dataIsSet = True;

		self.adjacencyMatrix = [];
		self.nodesAmount = 0;

		for connection in lineList:
			if (connection[0] > self.nodesAmount):
				self.nodesAmount = connection[0]
			if (connection[1] > self.nodesAmount):
				self.nodesAmount = connection[1]
		
		self.nodesAmount += 1;

		for _ in range(self.nodesAmount):
			self.adjacencyMatrix.append([0] * self.nodesAmount);

		for line in lineList:
			weight = 1;

			if (len(line) > 2): 
				weight = line[2];

			self.adjacencyMatrix[line[0]][line[1]] = weight;
			self.adjacencyMatrix[line[1]][line[0]] = weight;

	def setUsingConnectionList(self, connectionList):
		self.dataIsSet = True;

		self.adjacencyMatrix = [];
		self.nodesAmount = len(connectionList)

		for _ in range(self.nodesAmount):
			self.adjacencyMatrix.append([0] * self.nodesAmount);
			
		index = 0;

		for connections in connectionList:
			for connection in connections:
				weight = 1;
				connectionIndex = connection;

				if (isinstance(connection, list)):
					weight = connection[1];
					connectionIndex = connection[0]

				self.adjacencyMatrix[index][connectionIndex] = weight;
				self.adjacencyMatrix[connectionIndex][index] = weight;
			
			index += 1;

	def minimumDistance(self, distance, usedVertices):
		minimum = float('inf');
		minimumNode = 0;

		for u in range(self.nodesAmount):
			if distance[u] < minimum and usedVertices[u] == False:
				minimum = distance[u]
				minimumNode = u

		return minimumNode

	def shortestPath(self, fromNode, toNode):
		if (self.dataIsSet == False):
			raise Exception("No data has been set");

		distance = [float('inf')] * self.nodesAmount
		distance[fromNode] = 0
		usedVertices = [False] * self.nodesAmount

		for _ in range(self.nodesAmount):

			x = self.minimumDistance(distance, usedVertices)

			usedVertices[x] = True

			for y in range(self.nodesAmount):
				if self.adjacencyMatrix[x][y] > 0 and usedVertices[y] == False and distance[y] > (distance[x] + self.adjacencyMatrix[x][y]):
						distance[y] = distance[x] + self.adjacencyMatrix[x][y]

		return distance[toNode];
