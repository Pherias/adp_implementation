﻿/**
 * In this file you'll find an example of implementing generics in C# since implementing generics is required for the final product.
 * 
 * The example I will be using is a class 'Bucket' in which you can place any item, regardless of its type.
 * Only ten items can be placed in the bucket, it can be emptied completely and you can take the item on top (like a Stack)
 * 
 * This is just an example of implementing Generics and has no real use-case.
 */

namespace GenericsImplementation
{
    internal class Bucket<T>
    {
        private const int maxItemsAmount = 10;

        private T[] items;
        private int index;

        public Bucket()
        {
            this.items = new T[maxItemsAmount];
            this.index = 0;
        }

        public void Add(T item)
        {
            if (index == maxItemsAmount)
                throw new System.Exception("Bucket is full, empty it first");

            items[index] = item;
            index += 1;
        }

        public T TakeItemOnTop()
        {
            if (index == 0)
                throw new System.Exception("Bucket is empty, fill it up first");

            T item = items[index - 1];

            items[index - 1] = default;
            index -= 1;

            return item;
        }

        public void Empty()
        {
            this.items = new T[maxItemsAmount];
            this.index = 0;
        }
    }
}