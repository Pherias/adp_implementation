# Dynamic array implemented using a Java method where you have to initialize specific array lengths.
# The method getEmptyArray simply mocks initializing this fixed length
class List2:

    def __init__(self):
        self.length = 0;
        self.currentIndex = 0;
        self.staticArrayLength = 10;
        self.items = self.getEmptyArray(self.staticArrayLength);

    def add(self, value):
        if (self.currentIndex >= self.staticArrayLength):
            self.increaseArrayLength();

        self.items[self.currentIndex] = value;
        self.length += 1;
        self.currentIndex += 1;

    def remove(self, value):
        index = 0;

        for item in self.items:
            if item == value:
                self.removeIndex(index);
                return;

            index += 1;

    def removeIndex(self, index):
        self.items[index] = None;
        self.length -= 1;

        if (self.length == 0):
            self.clear();
        else:
            for i in range(index + 1, self.length):
                self.items[i - 1] = self.items[i];
    
    def isEmpty(self):
        return self.length == 0;

    def clear(self):
        self.items = self.getEmptyArray(self.staticArrayLength);
        self.length = 0;
        self.currentIndex = 0;

    def get(self, index):
        if index >= self.currentIndex:
            raise Exception("Index does not exist")
        return self.items[index]

    def increaseArrayLength(self):
        copy = self.items;

        self.staticArrayLength *= 2;
        self.items = self.getEmptyArray(self.staticArrayLength);

        index = 0;

        for item in copy:
            self.items[index] = item;

    # creates an empty array of a fixed length, mocking initializing statiic arrays like Java does.
    def getEmptyArray(self, length):
        items = [];

        for _ in range(length):
            items.append(None);
        
        return items;
