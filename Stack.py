from LinkedList import LinkedList;

class Stack:

    def __init__(self):
        self.list = LinkedList();
        self.length = 0;

    def pop(self):
        if (self.isEmpty()):
            raise Exception("Stack is empty");

        value = self.list.tail.value;

        self.list.removeItem(self.list.tail);

        self.length -= 1;

        return value;

    def add(self, item):
        self.length += 1;
        self.list.addAfterTail(item);
    
    def peak(self):
        if (self.isEmpty()):
            raise Exception("Stack is empty");

        return self.list.tail.value;

    def isEmpty(self):
        return self.list.length == 0;