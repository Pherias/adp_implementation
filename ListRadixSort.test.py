import unittest
import random;

from List import List;

class TestList(unittest.TestCase):
    
    def test_radixSortIntegers(self):
        list = List();

        for x in range(0, 100):
            list.add(random.randint(100, 1000))

        list.radixSort();

        for x in range(1, 100):
            self.assertGreaterEqual(list.get(x), list.get(x - 1));

if __name__ == '__main__':
    unittest.main()