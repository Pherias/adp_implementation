from List import List;

class Queue:

    list;

    def __init__(self):
        self.list = List();
        self.length = 0;
    
    def enqueue(self, item):
        self.list.add(item);
        self.length += 1;
        return;
    
    def dequeue(self):
        if (self.isEmpty()):
            raise Exception("Queue is empty");

        item = self.list.getFirst();
        
        self.length -= 1;
        self.list.removeFirst();

        return item;
    
    def peak(self):
        if (self.isEmpty()):
            raise Exception("Queue is empty");
        return self.list.getFirst();

    def isEmpty(self):
        return self.list.isEmpty();