from AVLTree import AVLTree

class PriorityQueue:

    def __init__(self):
        self.list = []
        self.tree = AVLTree();
        self.root = None;
        self.length = 0;

    def enqueue(self, value):
        self.root = self.tree.insert(self.root, value);
        self.length += 1;

    def peak(self):
        item = self.getItemWithHighestPriority();

        if (item == None):
            raise Exception('Queue is empty');

        return item.value;

    def dequeue(self):
        item = self.getItemWithHighestPriority();

        if (item == None):
            raise Exception('Queue is empty');

        self.root = self.tree.remove(self.root, item.value)

        return item.value;

    def getItemWithHighestPriority(self):
        if (self.root == None):
            return None;
       
        def recursion(root):
            if (root.right == None):
                return root;
            else:
                return recursion(root.right);

        highestValue = recursion(self.root);

        return highestValue;

    def getItemWithLowestPriority(self):
        if (self.root == None):
            return None;

        def recursion(root):
            if (root.left == None):
                return root;
            else:
                return recursion(root.left);

        lowestValue = recursion(self.root);

        return lowestValue;

class Item:

    def __init__(self, value, priority):
        self.value = value;
        self.priority = priority;