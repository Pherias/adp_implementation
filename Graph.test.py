import unittest
from DataSet import DataSet

from Graph import Graph;

class TestGraphAndShortestPath(unittest.TestCase):

    def test_errorWhenNoDataIsSet(self):
        graph = Graph();

        self.assertRaises(Exception, graph.shortestPath, 0, 1);
    
    def test_graphWithAdjacencyMatrix(self):
        graph = Graph();

        adjacencyMatrix = [
            [0, 4, 0, 0, 0, 0, 0, 8, 0],
            [4, 0, 8, 0, 0, 0, 0, 11, 0],
            [0, 8, 0, 7, 0, 4, 0, 0, 2],
            [0, 0, 7, 0, 9, 14, 0, 0, 0],
            [0, 0, 0, 9, 0, 10, 0, 0, 0],
            [0, 0, 4, 14, 10, 0, 2, 0, 0],
            [0, 0, 0, 0, 0, 2, 0, 1, 6],
            [8, 11, 0, 0, 0, 0, 1, 0, 7],
            [0, 0, 2, 0, 0, 0, 6, 7, 0]
        ]

        graph.setUsingAdjacencyMatrix(adjacencyMatrix)

        self.assertEqual(graph.shortestPath(0, 1), 4);
        self.assertEqual(graph.shortestPath(0, 2), 12);
        self.assertEqual(graph.shortestPath(0, 3), 19);
        self.assertEqual(graph.shortestPath(0, 4), 21);
        self.assertEqual(graph.shortestPath(0, 5), 11);
        self.assertEqual(graph.shortestPath(0, 6), 9);
        self.assertEqual(graph.shortestPath(0, 7), 8);
        self.assertEqual(graph.shortestPath(0, 8), 14);

    def test_graphWithWeightedAdjacencyMatrix(self):
        graph = Graph();
        adjacencyMatrix = DataSet.GetGraphData()['verbindingsmatrix_gewogen'];

        graph.setUsingAdjacencyMatrix(adjacencyMatrix);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);


    def test_graphWithLineList(self):
        graph = Graph();
        lineList = DataSet.GetGraphData()['lijnlijst'];

        graph.setUsingLineList(lineList);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWithWeightedLineList(self):
        graph = Graph();
        lineList = DataSet.GetGraphData()['lijnlijst_gewogen'];

        graph.setUsingLineList(lineList);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 0), 99);
        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);

    def test_graphWithConnectionList(self):
        graph = Graph();
        connectionList = DataSet.GetGraphData()['verbindingslijst'];

        graph.setUsingConnectionList(connectionList);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWithConnectionList(self):
        graph = Graph();
        connectionList = DataSet.GetGraphData()['verbindingslijst'];

        graph.setUsingConnectionList(connectionList);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWithWeightedConnectionList(self):
        graph = Graph();
        connectionList = DataSet.GetGraphData()['verbindingslijst_gewogen'];

        graph.setUsingConnectionList(connectionList);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);

if __name__ == '__main__':
    unittest.main()