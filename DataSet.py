import json;

class DataSet:

    @staticmethod
    def GetSorterenData():
        file = open('dataset_sorteren.json');

        return json.load(file);

    @staticmethod
    def GetHashTableData():
        file = open('dataset_hashing.json');

        return json.load(file);
    
    @staticmethod
    def GetGraphData():
        file = open('dataset_grafen.json');

        return json.load(file);
        