import unittest
import random
import string

from HashTable import HashTable;

class TestHashTable(unittest.TestCase):
    
    def test_addingItems(self):
        hashTable = HashTable();

        hashTable.set('test1', 'value 1');
        hashTable.set('test2', 'value 2');
        hashTable.set('test3', 'value 3');
        hashTable.set('test4', 'value 4');

        self.assertEqual(hashTable.get('test1'), 'value 1')
        self.assertEqual(hashTable.get('test2'), 'value 2')
        self.assertEqual(hashTable.get('test3'), 'value 3')
        self.assertEqual(hashTable.get('test4'), 'value 4')

    def test_overwritingValues(self):
        hashTable = HashTable();

        hashTable.set('test1', 15);
        hashTable.set('test1', 23);

        self.assertEqual(hashTable.get('test1'), 23)

    def test_settingValuesWithSameHash(self):
        hashTable = HashTable();

        hashTable.set('ziyxl', 15);
        hashTable.set('hwzzc', 23);

        self.assertEqual(hashTable.get('ziyxl'), 15)
        self.assertEqual(hashTable.get('hwzzc'), 23)

    def test_gettingNonExistingKey(self):
        hashTable = HashTable();

        hashTable.set('test1', 15);

        self.assertRaises(Exception, hashTable.get, 'test2');

    def test_deleteKey(self):
        hashTable = HashTable();

        hashTable.set('test1', 15);

        self.assertEqual(hashTable.get('test1'), 15);

        hashTable.delete('test1');

        self.assertRaises(Exception, hashTable.get, 'test1');


    def test_deleteNonExistingKey(self):
        hashTable = HashTable();

        self.assertRaises(Exception, hashTable.delete, 'test1')


if __name__ == '__main__':
    unittest.main()