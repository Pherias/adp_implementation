class Node():
	def __init__(self, value):
		self.value = value
		self.left = None
		self.right = None
		self.height = 1

class AVLTree():

	def insert(self, root, value):
	
		if not root:
			return Node(value)
		elif value < root.value:
			root.left = self.insert(root.left, value)
		else:
			root.right = self.insert(root.right, value)

		root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.right))

		balanceFactor = self.getBalanceFactor(root)

		if balanceFactor > 1 and value <= root.left.value:
			return self.rotateRight(root)

		if balanceFactor < -1 and value > root.right.value:
			return self.rotateLeft(root)

		if balanceFactor > 1 and value > root.left.value:
			root.left = self.rotateLeft(root.left)
			return self.rotateRight(root)

		if balanceFactor < -1 and value <= root.right.value:
			root.right = self.rotateRight(root.right)
			return self.rotateLeft(root)

		return root

	def remove(self, root, key):
		if not root:
			return root;
 
		elif key < root.value:
			root.left = self.remove(root.left, key)

		elif key > root.value:
			root.right = self.remove(root.right, key)

		else:
			if root.left is None:
				temp = root.right
				root = None
				return temp

			elif root.right is None:
				temp = root.left
				root = None
				return temp

			temp = self.getSmallestValue(root.right)
			root.value = temp.value
			root.right = self.remove(root.right, temp.value)

		if root is None:
			return root

		root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.right))

		balanceFactor = self.getBalanceFactor(root)

		if balanceFactor > 1 and self.getBalanceFactor(root.left) >= 0:
			return self.rotateRight(root)

		if balanceFactor < -1 and self.getBalanceFactor(root.right) <= 0:
			return self.rotateLeft(root)

		if balanceFactor > 1 and self.getBalanceFactor(root.left) < 0:
			root.left = self.rotateLeft(root.left)
			return self.rotateRight(root)

		if balanceFactor < -1 and self.getBalanceFactor(root.right) > 0:
			root.right = self.rotateRight(root.right)
			return self.rotateLeft(root)

		return root

	def rotateLeft(self, root):

		right = root.right
		rightLeft = right.left

		right.left = root
		root.right = rightLeft

		root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.right))
		right.height = 1 + max(self.getHeight(right.left), self.getHeight(right.right))

		return right

	def rotateRight(self, root):

		left = root.left
		leftRight = left.right

		left.right = root
		root.left = leftRight

		root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.right))
		left.height = 1 + max(self.getHeight(left.left), self.getHeight(left.right))

		return left

	def getHeight(self, root):
		if not root:
			return 0

		return root.height

	def getBalanceFactor(self, root):
		if not root:
			return 0

		return self.getHeight(root.left) - self.getHeight(root.right)

	def getSmallestValue(self, root):
		if root is None or root.left is None:
			return root

		return self.getSmallestValue(root.left)

	def printPreOrderTraversal(self, root):
		if root==None:
			return

		print(root.value)

		self.printPreOrderTraversal(root.left)

		self.printPreOrderTraversal(root.right)