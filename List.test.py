import unittest

from List import List;

class TestList(unittest.TestCase):
    
    def test_addingItems(self):
        list = List();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.get(1), 'test2')
        self.assertEqual(list.get(2), 'test3')
        self.assertEqual(list.length, 3)

    def test_removingIndex(self):
        list = List();

        list.add('test1');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.length, 1);

        list.removeIndex(0);

        self.assertEqual(list.length, 0);

    def test_removingValue(self):
        list = List();

        list.add('test1');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.length, 1);

        list.remove('test1');

        self.assertEqual(list.length, 0);

    def test_gettingNonExisitingIndex(self):
        list = List();

        self.assertRaises(Exception, list.get, 0);

        list.add('test1');
        list.get(0);
        list.removeIndex(0);

        self.assertRaises(Exception, list.get, 0);

    def test_clearList(self):
        list = List();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.length, 3);

        list.clear();

        self.assertEqual(list.length, 0);
        self.assertRaises(Exception, list.get, 0);
        self.assertRaises(Exception, list.get, 1);
        self.assertRaises(Exception, list.get, 2);

    def test_removeFirst(self):
        list = List();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.get(1), 'test2')
        self.assertEqual(list.get(2), 'test3')

        list.removeFirst();

        self.assertEqual(list.length, 2);
        self.assertEqual(list.get(0), 'test2')
        self.assertEqual(list.get(1), 'test3')

    def test_removeFirstOnEmptyList(self):
        list = List();

        self.assertRaises(Exception, list.removeFirst);

    def test_removeLast(self):
        list = List();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.length, 3);
        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.get(1), 'test2')
        self.assertEqual(list.get(2), 'test3')

        list.removeLast();

        self.assertEqual(list.length, 2);
        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.get(1), 'test2')

    def test_removeLastOnEmptyList(self):
        list = List();

        self.assertRaises(Exception, list.removeLast);

    def test_isEmpty(self):
        list = List();

        list.add('test1');
        list.add('test2');

        self.assertEqual(list.isEmpty(), False);

        list.removeIndex(0);
        list.removeIndex(0);

        self.assertEqual(list.isEmpty(), True);

    def test_slicing(self):
        list = List();

        list.add('test1')
        list.add('test2')
        list.add('test3')
        list.add('test4')
        list.add('test5')

        list.slice(1, 3);

        self.assertEqual(list.get(0), 'test2');
        self.assertEqual(list.get(1), 'test3');
        self.assertEqual(list.get(2), 'test4');
        self.assertEqual(list.length, 3);

    def test_slicingOverListLength(self):
        list = List();

        list.add('test1')
        list.add('test2')
        list.add('test3')
        list.add('test4')
        list.add('test5')

        list.slice(1, 10);

        self.assertEqual(list.get(0), 'test2');
        self.assertEqual(list.get(3), 'test5');
        self.assertEqual(list.length, 4);

if __name__ == '__main__':
    unittest.main()