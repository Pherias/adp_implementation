import unittest

from AVLTree import AVLTree;

class TestBinaryTree(unittest.TestCase):
    
    def test_insertingChilds(self):
        tree = AVLTree()
        root = None

        root = tree.insert(root, 1)
        root = tree.insert(root, 2)
        root = tree.insert(root, 3)
        root = tree.insert(root, 4)
        root = tree.insert(root, 5)
        root = tree.insert(root, 6)

        self.assertEqual(root.value, 4)
        self.assertEqual(root.left.value, 2)
        self.assertEqual(root.left.left.value, 1)
        self.assertEqual(root.left.right.value, 3)
        self.assertEqual(root.right.value, 5)
        self.assertEqual(root.right.right.value, 6)

        self.assertEqual(tree.getBalanceFactor(root), 0);
        self.assertEqual(tree.getBalanceFactor(root.left), 0);
        self.assertEqual(tree.getBalanceFactor(root.right), -1);

    def test_removingChilds(self):
        tree = AVLTree()
        root = None

        root = tree.insert(root, 1)
        root = tree.insert(root, 2)
        root = tree.insert(root, 3)
        root = tree.insert(root, 4)
        root = tree.insert(root, 5)
        root = tree.insert(root, 6)

        tree.remove(root, 6)

        self.assertEqual(root.right.value, 5)
        self.assertEqual(root.right.right, None)

        tree.remove(root, 1);
        
        self.assertEqual(root.left.value, 2)
        self.assertEqual(root.left.left, None)



if __name__ == '__main__':
    unittest.main()