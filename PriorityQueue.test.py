import unittest

from PriorityQueue import PriorityQueue;

class TestPriorityQueue(unittest.TestCase):
    
    def test_addItemsToQueue(self):
        queue = PriorityQueue();

        queue.enqueue(234);
        queue.enqueue(2);
        queue.enqueue(344);

        value = queue.peak();

        self.assertEqual(value, 344);
        self.assertEqual(queue.length, 3)

    def test_highestAndLowestValueIsCorrect(self):
        queue = PriorityQueue();

        queue.enqueue(234);
        queue.enqueue(2);
        queue.enqueue(344);
        queue.enqueue(3444);
        queue.enqueue(1294);
        queue.enqueue(54);

        self.assertEqual(queue.getItemWithHighestPriority().value, 3444)
        self.assertEqual(queue.getItemWithLowestPriority().value, 2)

    def test_dequeueingAnItem(self):
        queue = PriorityQueue();

        queue.enqueue(234);
        queue.enqueue(422);
        queue.enqueue(122)

        value = queue.dequeue();
        peakedValue = queue.peak();
        dequeuedValue = queue.dequeue();

        self.assertEqual(value, 422);
        self.assertEqual(peakedValue, 234);
        self.assertEqual(dequeuedValue, 234);

    def test_queueingAndDequeueing(self):
        queue = PriorityQueue();

        queue.enqueue(113);
        queue.enqueue(312);
        queue.enqueue(533);

        value = queue.dequeue();
        peakedValue = queue.peak();

        self.assertEqual(value, 533);
        self.assertEqual(peakedValue, 312);

        secondDequeue = queue.dequeue();
        self.assertEqual(secondDequeue, 312);

        thirdDequeue = queue.dequeue();
        self.assertEqual(thirdDequeue, 113);
    
    def test_peakingEmptyQueueException(self):
        queue = PriorityQueue();

        self.assertRaises(Exception, queue.peak);

    def test_dequeueingEmptyQueueException(self):
        queue = PriorityQueue();

        self.assertRaises(Exception, queue.dequeue);


if __name__ == '__main__':
    unittest.main()