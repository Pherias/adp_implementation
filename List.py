from math import isnan


class List:

    items = [];
    length = 0;

    def __init__(self):
        self.items = [];
        self.length = 0;

    def add(self, item):
        self.length += 1;
        self.items.append(item);

    def remove(self, item):
        if (self.isEmpty()):
            raise Exception("List is empty");

        self.items.remove(item);

        self.length -= 1;
    
    def removeIndex(self, index):
        if (self.isEmpty()):
            raise Exception("List is empty");
        if (self.length < index - 1):
            raise Exception("Index " + index + " not found in list");

        self.items[index : index + 1] = [];
    
        self.length -= 1;
    
    def clear(self):
        self.items = [];
        self.length = 0;

    def removeLast(self):
        if (self.isEmpty()):
            raise Exception("List is empty");

        self.removeIndex(self.length - 1);

    def removeFirst(self):
        if (self.isEmpty()):
            raise Exception("List is empty");

        self.removeIndex(0);

    def get(self, index):
        if (index < 0 or index >= self.length):
            raise Exception("Index out of range");

        return self.items[index];

    def getFirst(self):
        if (self.isEmpty()):
            raise Exception("List is empty");

        return self.items[0];

    def getLast(self):
        if (self.isEmpty()):
            raise Exception("List is empty");

        return self.items[self.length - 1];

    def isEmpty(self):
        return self.length == 0;

    def slice(self, start, end):
        self.items = self.items[start : end + 1];
        self.length = len(self.items);

    # Sorts the current list using the insertion sort algorithm
    # Param comparator - method with two parameters by which the sorting logic is determined.
    # Returns None
    def insertionSort(self, comparator):
        self.items = List.runInsertionSort(self.items, comparator);
    
    # Sorts the provided array using the insertion sort.
    # Param array - the array to sort
    # Param comparator - method with two paramters by which the sorting logic is determined
    # Returns the sorted array
    @staticmethod
    def runInsertionSort(array, comparator):
        arrayLength = len(array);
        if (arrayLength == 0):
            return [];

        indicesToSort = range(1, arrayLength);

        for i in indicesToSort:
            while comparator(array[i - 1], array[i]) and i > 0:
                array[i], array[i - 1] = array[i - 1], array[i];
                i -= 1;

        return array;

    # Sorts the current list using the selection sort algorithm
    # Param comparator - method with two parameters by which the sorting logic is determined
    # Returns None
    def selectionSort(self, comparator):
        for i in range(0, self.length - 1):

            minimum = i;

            for j in range(i + 1, self.length):
                if (comparator(self.items[minimum], self.items[j])):
                    minimum = j;
        
            if (minimum != i):
                self.items[i], self.items[minimum] = self.items[minimum], self.items[i];

    # Sorts the current list using the quick sort algorithm
    # Returns None
    def quickSort(self):
        
        def partition(leftBound, rightBound):
            i = leftBound;
            j = rightBound - 1;
            pivot = self.items[rightBound];

            while i < j:
                while i < rightBound and self.items[i] < pivot:
                    i += 1;
                while j > leftBound and self.items[j] >= pivot:
                    j -= 1;
                if (i < j):
                    self.items[i], self.items[j] = self.items[j], self.items[i];

            if (self.items[i] > pivot):
                self.items[i], self.items[rightBound] = self.items[rightBound], self.items[i];

            return i;
        
        def recursion(leftBound, rightBound):
            if (leftBound < rightBound):
                pivot = partition(leftBound, rightBound);
                recursion(leftBound, pivot - 1);
                recursion(pivot + 1, rightBound);

        recursion(0, self.length - 1);

    # Sorts the current list using the paralel merge sort algorithm
    # Param comparator
    # Returns None
    def mergeSort(self, comparator):
        self.items = List.runMergeSort(self.items, comparator);
        
    # Sorts the provided array using the paralel merge sort.
    # Param array - the array to sort
    # Param comparator - method with two paramters by which the sorting logic is determined
    # Returns the sorted array
    @staticmethod
    def runMergeSort(array, comparator):
        def recursion(array):
            if (len(array) < 2):
                return array;

            mid = int(len(array) / 2);

            leftArrayCopy = array[0 : mid];
            rightArrayCopy = array[mid : len(array)];

            left = recursion(leftArrayCopy);
            right = recursion(rightArrayCopy);

            return List.merge(left, right, comparator);

        return recursion(array)

    # Merges the two privided arrays using the provided sorting comparator logic
    # Param left - the first array
    # param right - the second array
    # Param comparator - method with two paramters by which the sorting logic is determined
    # Returns the sorted array
    @staticmethod
    def merge(left, right, comparator):
        array = [];
        leftLength = len(left);
        rightLength = len(right);
        indexLeft = 0;
        indexRight = 0;

        while indexLeft < leftLength and indexRight < rightLength:
            if comparator(right[indexRight], left[indexLeft]):
                array.append(left[indexLeft]);
                indexLeft += 1;
            else:
                array.append(right[indexRight]);
                indexRight += 1;
        
        while indexLeft < leftLength:
            array.append(left[indexLeft]);
            indexLeft += 1;
        
        while indexRight < rightLength:
            array.append(right[indexRight]);
            indexRight += 1;

        return array;

    # Sorts the current items in the list using timsort sorting algorithm.
    # Param comparator -  method with two parameters which determines the sorting logic
    # Param run - length of the (first) sets timsort algorithm sorts and merges iteratively
    # Returns None
    def timSort(self, comparator, run):
        for x in range(0, self.length, run):
            self.items[x : x + run] = List.runInsertionSort(self.items[x : x + run], comparator);

        while run < self.length:
            for x in range(0, self.length, 2 * run):
                self.items[x : x + run * 2] = List.merge(self.items[x : x + run], self.items[x + run : x + run * 2], comparator);

            run *= 2;

    # Sorts the current items in the list using radix sorting algorithm. Only possible with numbers.
    # Returns None
    # Throws Exception - if items in the list are not numbers
    def radixSort(self):
        if self.length == 0:
            return;
        if isnan(self.items[0]):
            raise Exception("Only numbers can be sorted using the radix sort algorithm");

        def getHighestNumberOfDigitsInArray(array):
            maxDigits = 0;

            for item in array:
                maxDigits = max(item, maxDigits);
            
            return len(str(maxDigits));

        def flatten(buckets):
            items = [];
            for bucket in buckets:
                for item in bucket:
                    items.append(item);

            return items;
        
        maxDigits = getHighestNumberOfDigitsInArray(self.items);

        for digit in range(0, maxDigits):
            buckets = [[] for i in range(10)];
            for item in self.items:
                currentDigit = item // 10 ** (digit) % 10;
                buckets[currentDigit].append(item);
            self.items = flatten(buckets);
    
    # Searching for a value in the current list using the binary search algorithm.
    # Param value - targeted value to search for
    # Returns Boolean wether the value exists. 
    def binarySearch(self, value):
        
        def recursion(leftBound, rightBound):
            if (leftBound > rightBound):
                return False;
            
            center = int((leftBound + rightBound) / 2);

            if (self.items[center] == value):
                return True;
            elif (value < self.items[center]):
                return recursion(leftBound, center - 1)
            else:
                return recursion(center + 1, rightBound);

        return recursion(0, self.length - 1);