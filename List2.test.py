import unittest

from List2 import List2;

class TestList(unittest.TestCase):
    
    def test_addingItems(self):
        list = List2();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.get(1), 'test2')
        self.assertEqual(list.get(2), 'test3')
        self.assertEqual(list.length, 3)

    def test_removingIndex(self):
        list = List2();

        list.add('test1');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.length, 1);

        list.removeIndex(0);

        self.assertEqual(list.length, 0);

    def test_removingValue(self):
        list = List2();

        list.add('test1');

        self.assertEqual(list.get(0), 'test1')
        self.assertEqual(list.length, 1);

        list.remove('test1');

        self.assertEqual(list.length, 0);

    def test_gettingNonExisitingIndex(self):
        list = List2();

        self.assertRaises(Exception, list.get, 0);

        list.add('test1');
        list.get(0);
        list.removeIndex(0);

        self.assertRaises(Exception, list.get, 0);

    def test_clearList(self):
        list = List2();

        list.add('test1');
        list.add('test2');
        list.add('test3');

        self.assertEqual(list.length, 3);

        list.clear();

        self.assertEqual(list.length, 0);
        self.assertRaises(Exception, list.get, 0);
        self.assertRaises(Exception, list.get, 1);
        self.assertRaises(Exception, list.get, 2);

    def test_isEmpty(self):
        list = List2();

        list.add('test1');
        list.add('test2');

        self.assertEqual(list.isEmpty(), False);

        list.removeIndex(0);
        list.removeIndex(0);

        self.assertEqual(list.isEmpty(), True);

    def test_increasingArrayLength(self):
        list = List2();

        for i in range(100):
            list.add(i);

            if (i == 19):
                self.assertEqual(len(list.items), 20)

            if (i == 29):
                self.assertEqual(len(list.items), 40)

            if (i == 59):
                self.assertEqual(len(list.items), 80)

            if (i == 100):
                self.assertEqual(len(list.items), 160)

if __name__ == '__main__':
    unittest.main()