import unittest

from Deque import Deque;

class TestPriorityQueue(unittest.TestCase):
    
    def test_addingItems(self):
        deque = Deque();

        deque.insertRight(53);
        deque.insertLeft(23);
        deque.insertRight(122);
        deque.insertLeft(222)

        mostRightValue = deque.peakRight();
        mostLeftValue = deque.peakLeft();

        self.assertEqual(deque.length, 4);
        self.assertEqual(mostRightValue, 122);
        self.assertEqual(mostLeftValue, 222);

    def test_removingLeftAndRight(self):
        deque = Deque();

        deque.insertRight(53);
        deque.insertLeft(23);
        deque.insertRight(122);
        deque.insertLeft(222);

        deque.removeLeft();
        deque.removeRight();

        mostRightValue = deque.peakRight();
        mostLeftValue = deque.peakLeft();

        self.assertEqual(deque.length, 2);
        self.assertEqual(mostRightValue, 53);
        self.assertEqual(mostLeftValue, 23);

    def test_removingRightEndsOnLeftInserted(self):
        deque = Deque();

        deque.insertRight(53);
        deque.insertLeft(23);
        deque.insertRight(122);
        deque.insertLeft(222);
    
        deque.removeRight();
        deque.removeRight();

        mostRight = deque.peakRight();

        self.assertEqual(mostRight, 23);

        deque.removeRight();

        mostRight = deque.peakRight();

        self.assertEqual(mostRight, 222);

    def test_peakingLeftOnEmpty(self):
        deque = Deque();
        deque.insertRight(53);
        deque.insertLeft(23);
        deque.removeRight();
        deque.removeLeft();

        self.assertRaises(Exception, deque.peakLeft);

    
    def test_peakingRightOnEmpty(self):
        deque = Deque();
        deque.insertRight(53);
        deque.insertLeft(23);
        deque.removeRight();
        deque.removeLeft();

        self.assertRaises(Exception, deque.peakRight);

    
    def test_removingLeftOnEmpty(self):
        deque = Deque();
        deque.insertRight(53);
        deque.insertLeft(23);
        deque.removeRight();
        deque.removeLeft();

        self.assertRaises(Exception, deque.removeLeft);

    
    def test_removingRightOnEmpty(self):
        deque = Deque();
        deque.insertRight(53);
        deque.insertLeft(23);
        deque.removeRight();
        deque.removeLeft();

        self.assertRaises(Exception, deque.removeRight);


if __name__ == '__main__':
    unittest.main()