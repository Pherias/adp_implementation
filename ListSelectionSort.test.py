import unittest

from List import List;

class TestList(unittest.TestCase):
    
    def test_selectionSortIntegers(self):
        list = List();

        list.add(5);
        list.add(1);
        list.add(7);
        list.add(6);
        list.add(8);
        list.add(3);

        def comparator(a, b):
            return a >= b

        list.selectionSort(comparator);

        self.assertEqual(list.get(0), 1);
        self.assertEqual(list.get(1), 3);
        self.assertEqual(list.get(2), 5);
        self.assertEqual(list.get(3), 6);
        self.assertEqual(list.get(4), 7);
        self.assertEqual(list.get(5), 8);

    def test_selectionSortFloats(self):
        list = List();

        list.add(5.343);
        list.add(1.222);
        list.add(7.66423);
        list.add(6.3434);
        list.add(6.543);
        list.add(1.221);

        def comparator(a, b):
            return a >= b

        list.selectionSort(comparator);

        self.assertEqual(list.get(0), 1.221);
        self.assertEqual(list.get(1), 1.222);
        self.assertEqual(list.get(2), 5.343);
        self.assertEqual(list.get(3), 6.3434);
        self.assertEqual(list.get(4), 6.543);
        self.assertEqual(list.get(5), 7.66423);
        
    def test_selectionSortStrings(self):
        list = List();

        list.add('3f');
        list.add('adfsdfa');
        list.add('asds');
        list.add('fgc');
        list.add('sadfa');
        list.add('asdfasdddd')

        def comparator(a, b):
            return len(a) > len(b)

        list.selectionSort(comparator);

        self.assertEqual(list.get(0), '3f');
        self.assertEqual(list.get(1), 'fgc');
        self.assertEqual(list.get(2), 'asds');
        self.assertEqual(list.get(3), 'sadfa');
        self.assertEqual(list.get(4), 'adfsdfa');
        self.assertEqual(list.get(5), 'asdfasdddd');

    def test_selectionSortClasses(self):
        list = List();

        class Car:
            def __init__(self, name, maxSpeed):
                self.name = name;
                self.maxSpeed = maxSpeed;

        car1 = Car('car 1', 190);
        car2 = Car('car 2', 65);
        car3 = Car('car 3', 310);
        car4 = Car('car 4', 45);
        car5 = Car('car 5', 240);

        list.add(car1);
        list.add(car2);
        list.add(car3);
        list.add(car4);
        list.add(car5);

        def comparator(a, b):
            return a.maxSpeed > b.maxSpeed;
        
        list.selectionSort(comparator);

        self.assertEqual(list.get(0), car4);
        self.assertEqual(list.get(1), car2);
        self.assertEqual(list.get(2), car1);
        self.assertEqual(list.get(3), car5);
        self.assertEqual(list.get(4), car3);


if __name__ == '__main__':
    unittest.main()