import unittest
import time

from AVLTree import AVLTree;
from DataSet import DataSet
from Deque import Deque;
from List import List
from PriorityQueue import PriorityQueue;
from Stack import Stack;
from LinkedList import LinkedList;
from Queue import Queue;
from HashTable import HashTable;
from Graph import Graph;

class TestPriorityQueue(unittest.TestCase):
    
    # LIST

    def test_list(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);
        
        self.assertEqual(list.length, 9999);
        self.assertEqual(list.get(5), 9302);
        self.assertEqual(list.getFirst(), 5824);
        self.assertEqual(list.getLast(), 8009);

    # Stack
    def test_stack(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        stack = Stack();

        for item in array:
            stack.add(item);

        self.assertEqual(stack.length, 9999)
        self.assertEqual(stack.peak(), 8009);

        poppedItem = stack.pop();

        self.assertEqual(poppedItem, 8009);
        self.assertEqual(stack.peak(), 5798);

    # (Doubly) Linked List
    def test_doublyLinkedList(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];
        
        list = LinkedList();

        for item in array:
            list.addAfterTail(item);

        self.assertEqual(list.head.value, 5824);
        self.assertEqual(list.tail.value, 8009);
        self.assertEqual(list.get(10).value, 2379);
        self.assertEqual(list.get(11).value, 2575);
        self.assertEqual(list.get(12).value, 5844);

        list.addAfterIndex(10, 15);

        self.assertEqual(list.get(10).value, 2379);
        self.assertEqual(list.get(10).next.value, 15);
        self.assertEqual(list.get(11).value, 15);
        self.assertEqual(list.get(11).previous.value, 2379);
        self.assertEqual(list.get(11).next.value, 2575);
        self.assertEqual(list.get(12).value, 2575);
        self.assertEqual(list.get(12).previous.value, 15);

    # Deque
    def test_deque(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        deque = Deque();

        for item in array:
            deque.insertRight(item);

        self.assertEqual(deque.peakLeft(), 5824);
        self.assertEqual(deque.peakRight(), 8009);
        
        deque.removeLeft()

        self.assertEqual(deque.peakLeft(), 8599);

        deque.removeLeft()

        self.assertEqual(deque.peakLeft(), 4899);

        deque.removeLeft()
        
        self.assertEqual(deque.peakLeft(), 4138);

    # Priority Queue
    def test_priorityQueue(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        queue = PriorityQueue();

        for item in array:
            queue.enqueue(item);

        self.assertEqual(queue.dequeue(), 9999);
        self.assertEqual(queue.dequeue(), 9998);
        self.assertEqual(queue.dequeue(), 9997);
        self.assertEqual(queue.dequeue(), 9996);
    
    Queue
    def test_queue(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        queue = Queue();

        for item in array:
            queue.enqueue(item);

        self.assertEqual(queue.dequeue(), 5824);
        self.assertEqual(queue.dequeue(), 8599);
        self.assertEqual(queue.dequeue(), 4899);

    # Algorithms
    # Sorting

    def test_insertionSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        def comparator(a, b):
            return a > b;

        before = time.time();

        list.insertionSort(comparator);

        after = time.time();

        duration = after - before;

        print('\nInsertion sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));


    def test_selectionSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        def comparator(a, b):
            return a > b;

        before = time.time();

        list.selectionSort(comparator);

        after = time.time();

        duration = after - before;

        print('\nSelection sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));

    def test_mergeSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        def comparator(a, b):
            return a > b;

        before = time.time();

        list.mergeSort(comparator);

        after = time.time();

        duration = after - before;

        print('\nMerge sort sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));

    def test_quickSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        before = time.time();

        list.quickSort();

        after = time.time();

        duration = after - before;

        print('\nQuick sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));

    def test_radixSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        before = time.time();

        list.radixSort();

        after = time.time();

        duration = after - before;

        print('\nRadix sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));
        
    def test_timSort(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        def comparator(a, b):
            return a > b;

        before = time.time();

        list.timSort(comparator, 5);

        after = time.time();

        duration = after - before;

        print('\nTim sort took: ' + str(duration) + 's')

        for i in range(1, list.length):
            self.assertGreater(list.get(i), list.get(i - 1));

    # Searching

    def test_binarySearch(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        list = List();

        for item in array:
            list.add(item);

        list.radixSort();

        before = time.time();

        foundValue = list.binarySearch(4039)

        after = time.time();

        duration = after - before;

        print('\nBinarysearch took: ' + str(duration) + 's')

        self.assertEqual(foundValue, True)

    def test_AVLTree(self):
        data = DataSet.GetSorterenData();
        array = data['lijst_willekeurig_10000'];

        tree = AVLTree();
        root = None;

        for item in array:
            root = tree.insert(root, item);

        def recursion(node):
            balanceFactor = tree.getBalanceFactor(node);
            
            if (node.left != None and node.right == None):
                self.assertEqual(balanceFactor, 1);
            elif (node.left == None and node.right != None):
                self.assertEqual(balanceFactor, -1);


            if (node.left != None):
                recursion(node.left);
            if (node.right != None):
                recursion(node.right);
        
        recursion(root);

    def test_hashTable(self):
        data = DataSet.GetHashTableData();
        items = data['hashtabelsleutelswaardes'];

        hashTable = HashTable();

        for item in items.items():
            key = item[0];
            value = item[1]

            hashTable.set(key, value);

        self.assertEqual(hashTable.get('a'), [0])
        self.assertEqual(hashTable.get('b'), [1])
        self.assertEqual(hashTable.get('c'), [2])
        self.assertEqual(hashTable.get('z0'), [99])
        self.assertRaises(Exception, hashTable.get, 'aa');

    def test_graphAdjacencyMatrix(self):
        data = DataSet.GetGraphData();
        adjacencyMatrix = data['verbindingsmatrix']

        graph = Graph();

        graph.setUsingAdjacencyMatrix(adjacencyMatrix);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphAdjacencyMatrix(self):
        data = DataSet.GetGraphData();
        adjacencyMatrix = data['verbindingsmatrix']

        graph = Graph();
        graph.setUsingAdjacencyMatrix(adjacencyMatrix);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWeightedAdjacencyMatrix(self):
        graph = Graph();
        dataSet = DataSet.GetGraphData();
        adjacencyMatrix = dataSet['verbindingsmatrix_gewogen'];

        graph.setUsingAdjacencyMatrix(adjacencyMatrix);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);

    def test_graphLinelist(self):
        data = DataSet.GetGraphData();
        lineList = data['lijnlijst']

        graph = Graph();
        graph.setUsingLineList(lineList);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWeightedLineList(self):
        graph = Graph();
        dataSet = DataSet.GetGraphData();
        lineList = dataSet['lijnlijst_gewogen'];

        graph.setUsingLineList(lineList);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 0), 99);
        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);

    def test_graphConnectionList(self):
        graph = Graph();
        dataSet = DataSet.GetGraphData();

        connectionList = dataSet['verbindingslijst'];

        graph.setUsingConnectionList(connectionList);

        self.assertEqual(graph.shortestPath(0, 1), 1);
        self.assertEqual(graph.shortestPath(0, 2), 1);
        self.assertEqual(graph.shortestPath(0, 3), 2);
        self.assertEqual(graph.shortestPath(0, 4), 2);
        self.assertEqual(graph.shortestPath(0, 5), 3);
        self.assertEqual(graph.shortestPath(0, 6), 4);

        self.assertEqual(graph.shortestPath(4, 1), 2);
        self.assertEqual(graph.shortestPath(4, 2), 1);
        self.assertEqual(graph.shortestPath(4, 3), 1);
        self.assertEqual(graph.shortestPath(4, 0), 2);
        self.assertEqual(graph.shortestPath(4, 5), 1);
        self.assertEqual(graph.shortestPath(4, 6), 2);

    def test_graphWeightedConnectionList(self):
        graph = Graph();
        dataSet = DataSet.GetGraphData();
        
        connectionList = dataSet['verbindingslijst_gewogen'];

        graph.setUsingConnectionList(connectionList);

        self.assertEqual(graph.shortestPath(0, 1), 99);
        self.assertEqual(graph.shortestPath(0, 2), 50);
        self.assertEqual(graph.shortestPath(0, 3), 149);
        self.assertEqual(graph.shortestPath(0, 4), 149);

        self.assertEqual(graph.shortestPath(1, 2), 50);
        self.assertEqual(graph.shortestPath(1, 3), 50);
        self.assertEqual(graph.shortestPath(1, 4), 50);

if __name__ == '__main__':
    unittest.main()